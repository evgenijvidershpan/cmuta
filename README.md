# cmuta

C++ Micro Unit Test Asset (CMUTA)

macro UNIT_TEST_BEGIN(test_example) turn into

    static struct ut_test_example: unit_test {
        ut_test_example():unit_test("test_example"){
        try {

macro UNIT_TEST_ASSERT(foo(param)==78) turn into

    if (!(foo(param)==78)){
        print("%s:%d\n     assert(%s) failed!\n", __FILE__,__LINE__,
              "foo(param)==78");
        throw 0;
    }

macro UNIT_TEST_CHECK(foo(param)==78) turn into

    if (!(foo(param)==78)){
        ++ut_errors;
        print("%s:%d\n     check(%s) failed!\n", __FILE__,__LINE__,
              "foo(param)==78");
    }

and macro UNIT_TEST_END(test_example) turn into

        } catch(...){ ++ut_errors; }
        print("ends with %d errors.\n", ut_errors);
        };
    } ut_test_example;

using cmuta:
    
    #if UNIT_TEST
    UNIT_TEST_BEGIN(test_math)
    UNIT_TEST_CHECK(1+2==3)
    UNIT_TEST_END(test_math)
    #endif
