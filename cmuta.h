/* This file is a C++ Micro Unit Test Asset (CMUTA)
 Copyright © 2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN connection WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/** C++ Micro Unit Test Asset (CMUTA)

macro UNIT_TEST_BEGIN(test_example) turn into
----------------------------------------------------
static struct ut_test_example: unit_test {
    ut_test_example():unit_test("test_example"){
    try {
----------------------------------------------------

macro UNIT_TEST_ASSERT(foo(param)==78) turn into
----------------------------------------------------
if (!(foo(param)==78)){
    print("%s:%d\n     assert(%s) failed!\n", __FILE__,__LINE__,
          "foo(param)==78");
    throw 0;
}
----------------------------------------------------

macro UNIT_TEST_CHECK(foo(param)==78) turn into
----------------------------------------------------
if (!(foo(param)==78)){
    ++ut_errors;
    print("%s:%d\n     check(%s) failed!\n", __FILE__,__LINE__,
          "foo(param)==78");
}
----------------------------------------------------

and macro UNIT_TEST_END(test_example) turn into
----------------------------------------------------
    } catch(...){ ++ut_errors; }
    print("ends with %d errors.\n", ut_errors);
    };
} ut_test_example;
----------------------------------------------------
**/

#ifndef CMUTA_H_INC
#define CMUTA_H_INC

#if UNIT_TEST

#include <stdio.h>

struct unit_test {
    const char* const ut_name;
    unsigned int ut_errors;
    unit_test(const char* name) :ut_name(name), ut_errors(0){};
    int print(const char* format, ...){
        va_list orig;
        va_start(orig, format);
        printf("UT: [%s] ", ut_name);
        int rc = vprintf(format, orig);
        va_end(orig);
        return rc;
    };
private:
    unit_test& operator=(const unit_test& other);
    unit_test(const unit_test& other);
};

#define UNIT_TEST_BEGIN(x) static struct ut_##x:unit_test{ut_##x():unit_test(#x){try{
#define UNIT_TEST_END(x) }catch(const char* e){print("assert(%s) failed!\n", e);\
            }catch(...){++ut_errors; print("unknown error!\n");}\
            print("ends with %d errors.\n", ut_errors);};} ut_##x;
#define UNIT_TEST_ASSERT(x)  if (!(x)){ print(\
            "%s:%d\n     assert(%s) failed!\n", __FILE__,__LINE__, #x); throw 0;}
#define UNIT_TEST_CHECK(x)   if (!(x)){ ++ut_errors; print(\
            "%s:%d\n     check(%s) failed!\n", __FILE__,__LINE__, #x); }

#endif  // #if UNIT_TEST
#endif // CMUTA_H_INC
